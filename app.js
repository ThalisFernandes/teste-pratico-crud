require('dotenv').config();
const mysql = require('mysql2');
const express = require('express');
const crud = require('./crud');

//Cria uma pool de conexões com o banco
global.pool = mysql.createPool({
  connectionLimit : 5,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASS
});

//Cria as rotas do CRUD de contato
let app = express();
app.post('/contato', async (req, res) => {
  let rs = await crud.create(req.body);
  res.status(200).json(rs);
});
app.get('/contato', async (req, res) => {
  let rs = await crud.read(req.body);
  res.status(200).json(rs);
});
app.put('/contato', async (req, res) => {
  let rs = await crud.update(req.body);
  res.status(200).json(rs);
})
app.delete('/contato', async (req, res) => {
  let rs = await crud.delete(req.body);
  res.status(200).json(rs);
})

//Inicia o servidor
app.use(express.json());
app.listen(process.env.APP_PORT);